using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void PlayAgain()
    {
        Debug.Log("PLAY");
        SceneManager.LoadScene("SampleScene");
    }

    public void MainMenu()
    {
        Debug.Log("QUIT");
        SceneManager.LoadScene("MainMenu");
    }
}
