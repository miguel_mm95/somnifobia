using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Minijuego : MonoBehaviour
{
    string colorPulsado;
    string colorActual;
    public SpriteRenderer sr;
    public Sprite spriteRojo;
    public Sprite spriteVerde;
    public Sprite spriteAzul;

    void Start()
    {
        colorActual = "Rojo";
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void BotonPulsado(string colorPulsado)
    {
        if(colorPulsado == colorActual)
        {
            int numero = Random.Range(1, 4);
            switch (numero)
            {
                case (1):
                    colorActual = "Rojo";
                    sr.sprite = spriteRojo;
                    Debug.Log(colorActual);
                    break;
                case (2):
                    colorActual = "Azul";
                    sr.sprite = spriteAzul;
                    Debug.Log(colorActual);
                    break;

                case (3):
                    colorActual = "Verde";
                    sr.sprite = spriteVerde;
                    Debug.Log(colorActual);
                    break;
            }
        }

        else
        {
            SceneManager.LoadScene("SampleScene");
        }
    }
    
}
