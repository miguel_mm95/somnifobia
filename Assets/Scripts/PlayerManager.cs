using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public int cantSueno = 0;
    public int toxicidad = 0;
    public bool reset;

    GameObject reloj;
    GameObject efectoSueno;
    Reloj atribReloj;
    SleepEffect sleepEffect;

    void Awake()
    {
        reloj = GameObject.FindGameObjectWithTag("Reloj");
        atribReloj = reloj.GetComponent<Reloj>();

        efectoSueno = GameObject.FindGameObjectWithTag("Finish");
        sleepEffect = efectoSueno.GetComponent<SleepEffect>();
    }
    // Update is called once per frame
    void Update()
    {
        //Debug.Log(cantSueno);
        if(cantSueno >= 50 && cantSueno < 80)
        {
            sleepEffect.AjusteDof(120f, 1f);
            sleepEffect.AjusteVignette(0.7f, 0.1f);

        }else if(cantSueno >= 80)
        {
            sleepEffect.AjusteDof(240f, 1f);
            sleepEffect.AjusteVignette(0.85f, 0.1f);

        }else if(cantSueno < 50)
        {
            sleepEffect.AjusteVignette(0.35f, 0.25f);
            sleepEffect.AjusteDof(1f, 4f);

        }
    }

    public void CambiarStats(int sueno, int toxico)
    {
        if (cantSueno + sueno < 0)
        {
            cantSueno = 0;
        }
        else
        {
            cantSueno = cantSueno + sueno;
        }

        toxicidad += toxico;

        atribReloj.Reset();
    }
}
