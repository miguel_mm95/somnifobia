using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QTESys : MonoBehaviour
{
    public float fillAmount = 0;
    public float timeThreshold = 0;
    public float minusAmount = 0.2f;

    float sumAmount = 0.2f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("f"))
        {
            fillAmount += sumAmount;
        }
        timeThreshold += Time.deltaTime;

        if (timeThreshold > 0.05)
        {
            timeThreshold = 0;
            fillAmount -= minusAmount;
        }

        if(fillAmount < 0)
        {
            fillAmount = 0;
        }

        GetComponent<Image>().fillAmount = fillAmount;
    }
}
