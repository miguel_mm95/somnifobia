using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reloj : MonoBehaviour
{
    public float escalaTiempo = 1;

    private float tiempoDelFrame, tiempoAMostrar;
    private float sueno;

    GameObject player;
    PlayerManager atribPlayer;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        atribPlayer = player.GetComponent<PlayerManager>();
    }

    // Update is called once per frame
    void Update()
    {
        tiempoDelFrame = Time.deltaTime * escalaTiempo;
        tiempoAMostrar += tiempoDelFrame;

        if (atribPlayer.reset)
        {
            atribPlayer.cantSueno = 0;
        }
        else
        {
            atribPlayer.cantSueno = (int)tiempoAMostrar % 100;
        }
    }

    public void Reset()
    {
        tiempoAMostrar = 0;
    }
}
