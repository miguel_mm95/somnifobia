using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class SleepEffect : MonoBehaviour
{

    public PostProcessVolume volume;

    private Vignette _Vignette;
    private DepthOfField _Dof;


    // Start is called before the first frame update
    void Start()
    {
        volume.profile.TryGetSettings(out _Vignette);
        _Vignette.intensity.value = 0.35f;
        volume.profile.TryGetSettings(out _Dof);
        _Dof.focalLength.value = 1;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AjusteVignette(float valor, float velocidad)
    {

        _Vignette.intensity.value = Mathf.Lerp(_Vignette.intensity.value, valor, velocidad * Time.deltaTime);
    }

    public void AjusteDof(float valor, float velocidad)
    {

        _Dof.focalLength.value = Mathf.Lerp(_Dof.focalLength.value, valor, velocidad * Time.deltaTime);
    }
}
