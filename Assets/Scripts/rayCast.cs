using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class rayCast : MonoBehaviour
{
    public float distanciaRayo;
    public CanvasGroup canvasGroup;

    [SerializeField] public Material highlightPillMaterial;
    [SerializeField] public Material defaultPillMaterial;
    [SerializeField] public Material highlightDrinkMaterial;
    [SerializeField] public Material defaultDrinkMaterial;

    RaycastHit hit;
    [SerializeField] private string selectableObject = "Selectable";
    [SerializeField] private string puzzlePortatil = "Portatil";


    private Transform _selection;
    int sueno;
    int toxico;

    GameObject player;
    PlayerManager playerManager;


    void Awake(){
        player = GameObject.FindGameObjectWithTag("Player");
        playerManager = player.GetComponent<PlayerManager>();


    }

    // Update is called once per frame
    void Update()
    {
        if (_selection != null)
        {
            var selectionRenderer = _selection.GetComponent<Renderer>();

            if(_selection.name == "Pill")
            {
                selectionRenderer.material = defaultPillMaterial;
            }

            if (_selection.name == "Drink")
            {
                selectionRenderer.material = defaultDrinkMaterial; ;
            }

            _selection = null;
        }

        if(Physics.Raycast(this.transform.position, this.transform.forward, out hit, distanciaRayo))
        {
            //Esto es para cambiar el material
            //Debug.Log("Tocado");
            var selection = hit.transform;

            if(selection.CompareTag(selectableObject) || selection.CompareTag(puzzlePortatil))
            {
                canvasGroup.alpha = 1;

                var selectionRenderer = selection.GetComponent<Renderer>();
                if (selectionRenderer != null)
                {
                    if (selection.name == "Pill")
                    {
                        selectionRenderer.material = highlightPillMaterial;
                    }

                    if (selection.name == "Drink")
                    {
                        selectionRenderer.material = highlightDrinkMaterial;
                    }

                }
                _selection = selection;
                

                //Esta parte de c��digo es para la interacci��n con objetos
                if (Input.GetKeyDown("e"))
                {

                    if (hit.transform.gameObject.tag == "Selectable")
                    {
                        //Debug.Log(playerManager.cantSueno);
                        if (hit.transform.name == "Pill")
                        {
                            CPill item = hit.transform.gameObject.GetComponent<CPill>();
                            sueno = item.sueno;
                            toxico = item.toxico;
                        }
                        else if (hit.transform.name == "Drink")
                        {
                            CDrink item = hit.transform.gameObject.GetComponent<CDrink>();
                            sueno = item.sueno;
                            toxico = item.toxico;
                        }
                        
                        playerManager.CambiarStats(sueno, toxico);
                        //Debug.Log(playerManager.cantSueno);
                        //Destroy(hit.transform.gameObject);
                    }

                    if(hit.transform.gameObject.tag == "Portatil")
                    {
                        SceneManager.LoadScene("Minijuego");
                    }
                }
            }
            else
            {
                canvasGroup.alpha = 0;
            }

        }

    }
}
